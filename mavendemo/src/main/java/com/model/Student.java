package com.model;
//POJO
public class Student {

	private int studentNo;
	private String studentName;
	private float mark;

	public Student() {
		super();
	}

	public Student(int studentNo, String studentName, float mark) {
		super();
		this.studentNo = studentNo;
		this.studentName = studentName;
		this.mark = mark;
	}

	public int getStudentNo() {
		return studentNo;
	}

	public void setStudentNo(int studentNo) {
		this.studentNo = studentNo;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public float getMark() {
		return mark;
	}

	public void setMark(float mark) {
		this.mark = mark;
	}

}
