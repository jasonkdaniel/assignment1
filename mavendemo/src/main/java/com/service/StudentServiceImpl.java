package com.service;

import com.model.Student;

public class StudentServiceImpl implements StudentService {

	@Override
	public Student getStudentByName(Student[] students, String name) {
		//iterator 
		Student student = null;
		if (students != null && students.length > 0 &&  name !=null && name.isEmpty() ) {
			//logic 
			for (int i = 0; i < students.length; i++) {
				if(students[i].getStudentName().equalsIgnoreCase(name)) {
					student = students[i];
				}
			}
			
		} else {
			// Exception
		}
		
		return student;
	}

	@Override
	public Student getStudentByID(Student[] students, int studentId) {
		Student student = null;
		if (students != null && students.length > 0 &&   studentId > 0 ) {
			//logic 
			for (int i = 0; i < students.length; i++) {
				if(students[i].getStudentNo() == studentId) {
					student = students[i];
				}
			}
			
		} else {
			// Exception
		}
		return student;
	}

}
