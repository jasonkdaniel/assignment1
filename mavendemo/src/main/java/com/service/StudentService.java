package com.service;

import com.model.Student;

public interface StudentService {

	public abstract Student getStudentByName(Student[ ] students , String name );
	
	public abstract Student getStudentByID(Student[ ] students , int studentId);
}
