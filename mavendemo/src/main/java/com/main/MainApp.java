package com.main;

import com.model.Student;
import com.service.StudentService;
import com.service.StudentServiceImpl;

public class MainApp {

	public static void main(String[] args) {

		// dynamic data from user

		Student student1 = new Student(10, "One", 100f);
		Student student2 = new Student(20, "Two", 200f);
		Student student3 = new Student(30, "Three", 300f);
		Student student4 = new Student(40, "Four", 400f);

		Student[] students = new Student[4];
		students[0] = student1;
		students[1] = student2;
		students[2] = student3;
		students[3] = student4;

		// interface variableName = new InterfaceImplementation() == abstract ==exposing
		// method

		StudentService studentService = new StudentServiceImpl();
		Student obj = studentService.getStudentByID(students, 50);
		if (obj != null) {
			System.out.println("Student id   : " + obj.getStudentNo());
			System.out.println("Student name : " + obj.getStudentName());
		}
		else
		{
			System.out.println("No such student");
		}
	}

}
